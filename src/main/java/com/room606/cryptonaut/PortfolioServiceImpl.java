package com.room606.cryptonaut;

import com.room606.cryptonaut.domain.Portfolio;
import com.room606.cryptonaut.markets.CryptoMarketDataService;
import io.micronaut.context.annotation.Requires;
import io.reactiverse.pgclient.data.Numeric;
import io.reactiverse.reactivex.pgclient.PgIterator;
import io.reactiverse.reactivex.pgclient.PgPool;
import io.reactiverse.reactivex.pgclient.Row;
import io.reactiverse.reactivex.pgclient.Tuple;

import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Requires(notEnv="test")
public class PortfolioServiceImpl implements PortfolioService {

    private final PgPool pgPool;
    private final CryptoMarketDataService cryptoMarketDataService;

    private static final String UPDATE_COIN_AMT = "INSERT INTO portfolio (coin, amount) VALUES ($1, $2) ON CONFLICT (coin) " +
            "DO UPDATE SET amount = $3";

    private static final String SELECT_COINS_AMTS = "SELECT coin, amount FROM portfolio";

    @Inject
    public PortfolioServiceImpl(PgPool pgPool, CryptoMarketDataService cryptoMarketDataService) {
        this.pgPool = pgPool;
        this.cryptoMarketDataService = cryptoMarketDataService;
    }

    //TODO: Fix the bug, coins that are not in the json should be deleted. Maybe just write sql: delete & create ?
    public Portfolio savePortfolio(Portfolio portfolio) {
        List<Tuple> records = portfolio.getCoins()
                .entrySet()
                .stream()
                .map(entry -> Tuple.of(entry.getKey(), Numeric.create(entry.getValue()), Numeric.create(entry.getValue())))
                .collect(Collectors.toList());
        pgPool.preparedBatch(UPDATE_COIN_AMT, records, pgRowSetAsyncResult -> {
            pgRowSetAsyncResult.cause().printStackTrace();
        });
        return portfolio;
    }

    public Portfolio loadPortfolio() {
        Map<String, BigDecimal> coins = new HashMap<>();
        PgIterator pgIterator = pgPool.rxPreparedQuery(SELECT_COINS_AMTS).blockingGet().iterator();
        while (pgIterator.hasNext()) {
            Row row = pgIterator.next();
            coins.put(row.getString("coin"), new BigDecimal(row.getValue("amount").toString()));
        }

        Portfolio portfolio = new Portfolio();
        portfolio.setCoins(coins);
        return portfolio;
    }

    public Optional<BigDecimal> calculateTotalValue(Portfolio portfolio, String fiatCurrency) {
        return portfolio.getCoins()
                .entrySet()
                .stream()
                .map(coinEntry -> coinEntry.getValue().multiply(cryptoMarketDataService.getPrice(coinEntry.getKey(), fiatCurrency)))
                .reduce(BigDecimal::add);
    }
}
