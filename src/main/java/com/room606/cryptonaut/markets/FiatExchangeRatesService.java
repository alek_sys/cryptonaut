package com.room606.cryptonaut.markets;

import com.room606.cryptonaut.exceptions.NotSupportedFiatException;
import com.room606.cryptonaut.markets.clients.CbrExchangeRatesClient;
import io.micronaut.context.annotation.Value;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Singleton;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Singleton
public class FiatExchangeRatesService {

    @Value("${cryptonaut.currencies:RUR}")
    private String supportedCurrencies;
    private List<String> supportedCounterCurrencies;

    @PostConstruct
    void init() {
        supportedCounterCurrencies = Arrays.asList(supportedCurrencies.split("[,]", -1));
    }

    @Inject
    private CbrExchangeRatesClient cbrExchangeRatesClient;

    public BigDecimal getFiatPrice(String baseCurrency, String counterCurrency) throws NotSupportedFiatException {
        if (!supportedCounterCurrencies.contains(counterCurrency)) {
            throw new NotSupportedFiatException("Counter currency not supported: " + counterCurrency);
        }

        Map<String, BigDecimal> rates = cbrExchangeRatesClient.getRates();

        return rates.get(baseCurrency);
    }
}
