package com.room606.cryptonaut.markets;

import com.room606.cryptonaut.exceptions.CryptonautException;
import org.knowm.xchange.currency.Currency;
import org.knowm.xchange.currency.CurrencyPair;
import org.knowm.xchange.dto.marketdata.Ticker;
import org.knowm.xchange.exceptions.CurrencyPairNotValidException;
import org.knowm.xchange.service.marketdata.MarketDataService;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.math.BigDecimal;

@Singleton
public class CryptoMarketDataService {


    private final FiatExchangeRatesService fiatExchangeRatesService;

    private final MarketDataService marketDataService;

    @Inject
    public CryptoMarketDataService(FiatExchangeRatesService fiatExchangeRatesService, MarketDataServiceFactory marketDataServiceFactory) {
        this.fiatExchangeRatesService = fiatExchangeRatesService;
        this.marketDataService = marketDataServiceFactory.getMarketDataService();
    }

    public BigDecimal getPrice(String coinCode, String fiatCurrencyCode) throws CryptonautException {
        BigDecimal price = getPriceForBasicCurrency(coinCode, Currency.USD.getCurrencyCode());
        if (Currency.USD.equals(new Currency(fiatCurrencyCode))) {
            return price;
        } else {
            return price.multiply(fiatExchangeRatesService.getFiatPrice(Currency.USD.getCurrencyCode(), fiatCurrencyCode));
        }
    }

    private BigDecimal getPriceForBasicCurrency(String coinCode, String fiatCurrencyCode) throws CryptonautException {
        Ticker ticker = null;
        try {
            ticker = marketDataService.getTicker(new CurrencyPair(new Currency(coinCode), new Currency(fiatCurrencyCode)));
            return ticker.getBid();
        } catch (CurrencyPairNotValidException e) {
            ticker = getTicker(new Currency(coinCode), Currency.BTC);
            Ticker ticker2 = getTicker(Currency.BTC, new Currency(fiatCurrencyCode));
            return ticker.getBid().multiply(ticker2.getBid());
        } catch (IOException e) {
            throw new CryptonautException("Failed to get price for Pair " + coinCode + "/" + fiatCurrencyCode + ": " + e.getMessage(), e);
        }
    }

    private Ticker getTicker(Currency base, Currency counter) throws CryptonautException {
        try {
            return marketDataService.getTicker(new CurrencyPair(base, counter));
        } catch (CurrencyPairNotValidException | IOException e) {
            throw new CryptonautException("Failed to get price for Pair " + base.getCurrencyCode()
                    + "/" + counter.getCurrencyCode() + ": " + e.getMessage(), e);
        }
    }
}
