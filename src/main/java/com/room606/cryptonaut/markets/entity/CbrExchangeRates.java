package com.room606.cryptonaut.markets.entity;

public class CbrExchangeRates {
    private Valute Valute;

    public CbrExchangeRates.Valute getValute() {
        return Valute;
    }

    public void setValute(CbrExchangeRates.Valute valute) {
        Valute = valute;
    }

    public static final class Valute {
        private Currency USD;
        private Currency GPB;
        private Currency EUR;

        public Currency getUSD() {
            return USD;
        }

        public void setUSD(Currency USD) {
            this.USD = USD;
        }

        public Currency getGPB() {
            return GPB;
        }

        public void setGPB(Currency GPB) {
            this.GPB = GPB;
        }

        public Currency getEUR() {
            return EUR;
        }

        public void setEUR(Currency EUR) {
            this.EUR = EUR;
        }

    }

    public static final class Currency {
        private String Value;

        public String getValue() {
            return Value;
        }

        public void setValue(String value) {
            Value = value;
        }
    }
}
