package com.room606.cryptonaut.markets.clients;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.room606.cryptonaut.exceptions.CryptonautException;
import io.micronaut.cache.annotation.Cacheable;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.client.RxHttpClient;
import io.micronaut.http.client.annotation.Client;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Singleton
public class CbrExchangeRatesClient {

    private static final String CBR_DATA_URI = "https://www.cbr-xml-daily.ru/daily_json.js";

    @Client(CBR_DATA_URI) @Inject
    private RxHttpClient httpClient;

    private final ObjectReader objectReader = new ObjectMapper().reader();

    @Cacheable("fiat-rates")
    public Map<String, BigDecimal> getRates() {
        try {
            HttpRequest<?> req = HttpRequest.GET("");
            String response = httpClient.retrieve(req, String.class).blockingSingle();
            JsonNode json  = objectReader.readTree(response);
            String usdPrice = json.get("Valute").get("USD").get("Value").asText();
            String eurPrice = json.get("Valute").get("EUR").get("Value").asText();
            String gbpPrice = json.get("Valute").get("GBP").get("Value").asText();
            Map<String, BigDecimal> prices = new HashMap<>();
            prices.put("USD", new BigDecimal(usdPrice));
            prices.put("GBP", new BigDecimal(gbpPrice));
            prices.put("EUR", new BigDecimal(eurPrice));
            return prices;
        } catch (IOException e) {
            throw new CryptonautException("Failed to obtain exchange rates: " + e.getMessage(), e);
        }
    }
}
