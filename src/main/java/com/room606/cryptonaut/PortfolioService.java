package com.room606.cryptonaut;

import com.room606.cryptonaut.domain.Portfolio;

import java.math.BigDecimal;
import java.util.Optional;

public interface PortfolioService {
    Portfolio savePortfolio(Portfolio portfolio);
    Portfolio loadPortfolio();
    Optional<BigDecimal> calculateTotalValue(Portfolio portfolio, String fiatCurrency);
}