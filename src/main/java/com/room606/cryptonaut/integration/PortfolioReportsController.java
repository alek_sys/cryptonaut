package com.room606.cryptonaut.integration;

import com.room606.cryptonaut.PortfolioService;
import com.room606.cryptonaut.domain.Total;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Produces;
import io.micronaut.http.annotation.QueryValue;

import java.math.RoundingMode;

@Controller("/portfolio")
public class PortfolioReportsController {

    private final PortfolioService portfolioService;

    public PortfolioReportsController(PortfolioService portfolioService) {
        this.portfolioService = portfolioService;
    }

    @Get("/total")
    @Produces(MediaType.APPLICATION_JSON)
    public Total totalValueAsJson(@QueryValue("fiatCurrency") String fiatCurrency) {
        return new Total(fiatCurrency, portfolioService.loadPortfolio(),
                portfolioService.calculateTotalValue(portfolioService.loadPortfolio(), fiatCurrency)
                        .get().setScale(2, RoundingMode.CEILING));
    }
}
