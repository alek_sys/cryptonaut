package com.room606.cryptonaut.domain;

import java.math.BigDecimal;

public class Price {
    private String coin;
    private BigDecimal value;

    public Price(String coin, BigDecimal value) {
        this.coin = coin;
        this.value = value;
    }

    public String getCoin() {
        return coin;
    }

    public void setCoin(String coin) {
        this.coin = coin;
    }

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }
}
