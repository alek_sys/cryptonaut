package com.room606.cryptonaut.domain;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;

public class Portfolio {
    private Map<String, BigDecimal> coins = Collections.emptyMap();

    public Map<String, BigDecimal> getCoins() {
        return new TreeMap<>(coins);
    }

    public void setCoins(Map<String, BigDecimal> coins) {
        this.coins = coins;
    }
}
