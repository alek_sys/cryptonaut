package com.room606.cryptonaut;

import com.room606.cryptonaut.domain.Portfolio;
import io.micronaut.context.annotation.Requires;
import io.micronaut.context.env.Environment;

import javax.inject.Singleton;
import java.math.BigDecimal;
import java.util.Optional;

@Singleton
@Requires(env = Environment.TEST)
public class MockPortfolioService implements PortfolioService {

    private Portfolio portfolio;

    public static final BigDecimal TEST_VALUE = new BigDecimal("56.65");

    @Override
    public Portfolio savePortfolio(Portfolio portfolio) {
        this.portfolio = portfolio;
        return portfolio;
    }

    @Override
    public Portfolio loadPortfolio() {
        return portfolio;
    }

    @Override
    public Optional<BigDecimal> calculateTotalValue(Portfolio portfolio, String fiatCurrency) {
        return Optional.of(TEST_VALUE);
    }
}
