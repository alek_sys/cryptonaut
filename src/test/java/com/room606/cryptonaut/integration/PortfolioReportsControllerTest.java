package com.room606.cryptonaut.integration;

import com.room606.cryptonaut.PortfolioService;
import com.room606.cryptonaut.domain.Portfolio;
import com.room606.cryptonaut.domain.Total;
import io.micronaut.context.ApplicationContext;
import io.micronaut.http.HttpRequest;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.client.HttpClient;
import io.micronaut.runtime.server.EmbeddedServer;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import static com.room606.cryptonaut.MockPortfolioService.TEST_VALUE;
import static org.junit.Assert.assertEquals;

public class PortfolioReportsControllerTest extends IntegrationTestBase {
    private PortfolioService portfolioService;

    @Before
    public void setUp() {
        portfolioService = server.getApplicationContext().getBean(PortfolioService.class);
    }

    @Test
    public void total() {

        Portfolio portfolio = new Portfolio();
        Map<String, BigDecimal> coins = new HashMap<>();
        BigDecimal amt1 = new BigDecimal("570.05");
        BigDecimal amt2 = new BigDecimal("2.5");
        coins.put("XRP", amt1);
        coins.put("QTUM", amt2);

        portfolio.setCoins(coins);

        portfolioService.savePortfolio(portfolio);

        HttpRequest<Total> request = HttpRequest.GET("/portfolio/total?fiatCurrency=USD");

        HttpResponse<Total> rsp = client.toBlocking().exchange(request, Total.class);

        assertEquals(200, rsp.status().getCode());
        assertEquals(MediaType.APPLICATION_JSON_TYPE, rsp.getContentType().get());

        Total val = rsp.body();
        assertEquals("USD", val.getFiatCurrency());
        assertEquals(TEST_VALUE.toString(), val.getValue().toString());
        assertEquals(amt1.toString(), val.getPortfolio().getCoins().get("XRP").toString());
        assertEquals(amt2.toString(), val.getPortfolio().getCoins().get("QTUM").toString());
    }
}
